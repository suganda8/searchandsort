#include <iostream>
#include <stdlib.h>
#include <string>
#include <ctype.h>

// Written by Tegar Bangun Suganda.

using namespace std;

void bubbleSort(int* arrays, int length){
    int i, j, temp, pass = 0;

    cout << "\nPanjang Array : " << length << endl;

    cout << "\nIsi Array : " << endl;

    for(i=0; i<length; i++){
        cout << arrays[i] << endl;
    }

    cout << "\nMensortir ...\n" << endl;

    for(i=0; i<length; i++){
        for(j=i+1; j<length; j++){
            if(arrays[i] > arrays[j]){
            temp = arrays[i];
            arrays[i] = arrays[j];
            arrays[j] = temp;
            }
        }
        pass++;
    }

    cout << "Tersortir ... " << endl;
    for(i=0; i<length; i++){
        cout << arrays[i] << endl;
    }
};

void insertionSort(int* arrays, int length){
    cout << "\nPanjang Array : " << length << endl;
    cout << "\nIsi Array : " << endl;
    for(int i=0; i<length; i++){
        cout << arrays[i] << endl;
    }
    cout << "\nMensortir ...\n" << endl;

    //SelectionSort
    for(int i=0; i<length; i++){
        //cout << arrays[i] << endl;
        int temp = arrays[i];
        int j = i - 1;
        //cout << "TEMP : " << temp << endl;
        cout << "J : " << j << endl;
        while(j>=0 && temp <= arrays[j]){
            cout << "J + 1 = ARR[j] : " << arrays[j] << endl;
            //for(int iz=0; iz<length; iz++){
            //    cout << "---PRE--- " << arrays[iz] << endl;
            //}
            arrays[j+1] = arrays[j];
            //for(int iz=0; iz<length; iz++){
            //    cout << "---POST---" << arrays[iz] << endl;
            //}
            j--;
        }
        arrays[j+1] = temp;
        cout << "New Temp : " << arrays[j+1] << endl;
    }

    cout << "Tersortir ... " << endl;
    for(int i=0; i<length; i++){
        cout << arrays[i] << endl;
    }
};

int cinInteger(string str){
    int valueAngka = 0;
    string strAngka;
    while(true){
    cout << str; cin >> strAngka;
        try{
            valueAngka = stoi(strAngka);
            if(valueAngka == 0){
                cout << "Tolong masukkan hanya angka -----!" << endl;
            }else if(valueAngka < 0){
                cout << "Tolong masukkan hanya angka dengan nilai positif!" << endl;
            }else if(valueAngka > 0){
                //cout << "DEBUG" << endl;
                return valueAngka;
            }
        }catch(...){
            cout << "Tolong masukkan hanya angka ---!" << endl;
            cin.clear();
            cin.ignore();
        }
    }
};

int main(){
    int loops=0, inc = 0;
    int* arx = NULL;
    loops = cinInteger("Masukkan Berapa Kali Perulangan : ");
    arx = new int[loops];

    //cout << "Debug data " << sizeof() << endl;
    //int arr[] = new int[loops];
    //if(isdigit(loops)){
       do{
            cout << "Masukkan Bilangan ke-" << inc+1 << " : "; cin >> arx[inc];
            inc++;
            }while(inc < loops);

    //for(int i = 0; i < loops; i++){
       // cout << arx[i]<<endl;
    //}

    insertionSort(arx, loops);

    delete [] arx;
    arx = NULL;
    //}else{
    //    cout << "XAS";
    //}
}
